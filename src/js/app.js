/* eslint-disable radix */
/* eslint-disable func-names */
import svg4everybody from 'svg4everybody';
import {DOM} from './_const';

import './modules/_grid'
import dropInNav from './modules/_drop'
import bannerSlider from './modules/_banner-slider'
import './modules/_product-slider'
import './modules/_input-mask'


import './modules/_drop-cat'
import './modules/_default-tabs'
import './modules/_hover-card'

import './modules/_slider'

import './modules/_light-gallery'
import './modules/_niceScroll'

import './modules/_footer'

// eslint-disable-next-line import/extensions
import './modules/_map'

import './modules/_custom-select'

import './modules/_checkout-acc'

import './modules/_show-out-modal'

svg4everybody();


const onContentLoaded = () => {
  let lazyImages = [].slice.call(document.querySelectorAll('img.lazy'));

  const lazyLoad = () => {
    lazyImages.forEach((lazyImage) => {
      const topBreakpoint = lazyImage.classList.contains('lazy_last') ?
                            window.innerHeight + 500 : window.innerHeight + 200;

      if ((lazyImage.getBoundingClientRect().top <= topBreakpoint
          && lazyImage.getBoundingClientRect().bottom >= 0) && getComputedStyle(lazyImage).display !== 'none') {
        // eslint-disable-next-line
        lazyImage.src = lazyImage.dataset.src;
        // eslint-disable-next-line
        lazyImage.srcset = lazyImage.dataset.srcset;
        lazyImage.classList.remove('lazy');

        lazyImages = lazyImages.filter((image) => {
        return image !== lazyImage;
        });

        if (lazyImages.length === 0) {
          DOM.document.removeEventListener('scroll', lazyLoad);
          DOM.window.removeEventListener('resize', lazyLoad);
          DOM.window.removeEventListener('orientationchange', lazyLoad);
        }
      }
    });
  };

  DOM.doc.addEventListener('scroll', lazyLoad);
  DOM.win.addEventListener('resize', lazyLoad);
  DOM.win.addEventListener('orientationchange', lazyLoad);
}


$(document).ready(function () {

  $(".js-type-delivery input[type=radio]").change(function(){
      if ( $('.js-courier-label').is(":checked") ){
        $('.js-courier-item').removeClass('is-hide')
        // console.log(0)
      } else {
        $('.js-courier-item').addClass('is-hide')
        // console.log(1)
      }
      if ( $('.js-np-label').is(":checked") ){
        $('.js-np-item').removeClass('is-hide')
        // console.log(0)
      } else {
        $('.js-np-item').addClass('is-hide')
        // console.log(1)
      }

      $('.js-delivery-item').removeClass('is-hide')
      // console.log(3)
      // //or
      // if ( $(this).prop("checked") ){}   
      // //or
      // if ( $(this).is(":checked") ){}
  });

  
  // $(document).on("click",".modal-basket__item-close",function(e) {
  //   e.preventDefault()

  //   const $parrent = $(this).closest('.modal-basket__item')
    
  //   $parrent.remove()

  // });


  // $(document).on("click",".js-count-p-minus",function(e) {
  //   e.preventDefault()
  //   const $parrent= $(this).closest('.js-change-count')
  //   const $input = $parrent.find('input');

  //   // eslint-disable-next-line no-undef
  //   const currentVal = parseInt($input.val());
  //   // eslint-disable-next-line no-restricted-globals
  //   if (!isNaN(currentVal) && currentVal > 0) {             
  //       // eslint-disable-next-line no-undef
  //       $input.val(currentVal - 1);
  //   } else {
  //     $input.val(0);
  //   }

  // });

  // $(document).on("click",".js-count-p-plus",function(e) {
  //   e.preventDefault()
  //   const $parrent= $(this).closest('.js-change-count')
  //   const $input = $parrent.find('input');
  //    // eslint-disable-next-line no-undef
  //   const currentVal = parseInt($input.val());
  //   // eslint-disable-next-line no-restricted-globals
  //   if (!isNaN(currentVal)) {             
  //       // eslint-disable-next-line no-undef
  //       $input.val(currentVal + 1);
  //   } else {
  //     $input.val(0);
  //   }

  // });


});

const onLoad = () => {
  dropInNav.init();
  if ($('.js-banner-slider').length) {
    bannerSlider.init();
  }
  

}

$(DOM.doc).ready(onLoad);
DOM.doc.addEventListener('DOMContentLoaded', onContentLoaded);

