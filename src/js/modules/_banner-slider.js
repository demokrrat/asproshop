import {Swiper, Navigation, Pagination} from "swiper";

Swiper.use([Navigation, Pagination]);

const bannerSlider = new Swiper('.js-banner-slider', {
  loop: true,
   // If we need pagination
   pagination: {
    el: '.js-banner-pagination',
    clickable: true,
  },

  // Navigation arrows
  navigation: {
    nextEl: '.js-banner-slider .swiper-button-next',
    prevEl: '.js-banner-slider .swiper-button-prev',
  },
});
export default bannerSlider;