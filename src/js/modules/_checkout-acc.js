/* eslint-disable no-unused-vars */
/* eslint-disable prefer-const */
/* eslint-disable func-names */
import UIkit from 'uikit';

$(document).ready(function () {

  UIkit.accordion('.js-acc-checkout', {
    // toggle: '> .checkout-steps__head',
    content: '> .checkout-steps__body'
  });

  UIkit.util.on('.js-acc-checkout', 'shown', function () {
    // do something
    let $heightCheckOut = $('.js-acc-checkout').height();
    let $wrapperCheck = $('.checkout-wrapper');
    $wrapperCheck.height($heightCheckOut);

    let $wrap = $('.checkout-steps__item.uk-open').first();
    let $number = $wrap.find('.checkout-steps__number');
    let $edit = $wrap.find('.checkout-steps__edit');
    $number.removeClass('is-complete');
    $edit.addClass('is-hide');

    let $wrapSecond = $('.checkout-steps__item').last();
    let $btn = $('.js-go-next');

    if ( $wrapSecond.is('.uk-open') ) {
      $btn.removeClass('is-hidden')
    }


  });
  UIkit.util.on('.js-acc-checkout', 'hide', function () {
    // do something
    let $wrap = $('.checkout-steps__item').first();
    let $number = $wrap.find('.checkout-steps__number');
    let $edit = $wrap.find('.checkout-steps__edit');
    $number.addClass('is-complete');
    $edit.removeClass('is-hide');


    
  });


  UIkit.util.on('.js-acc-checkout', 'hidden', function () {
    // do something

    let $wrapSecond = $('.checkout-steps__item').last();
    let $btn = $('.js-go-next');
    if ( !$wrapSecond.is('.uk-open') ) {
      $btn.addClass('is-hidden')
    }
    
  });

  $(document).on("click",".js-show-step-2",function(e) {
    e.preventDefault()

    UIkit.accordion('.js-acc-checkout').toggle(1, true);

  });

  $(document).on("click",".js-show-step-1",function(e) {
    e.preventDefault()

    UIkit.accordion('.js-acc-checkout').toggle(0, true);

  });
  
  


  setTimeout(() => {
    
  let $heightCheckOut = $('.js-acc-checkout').height();
  let $wrapperCheck = $('.checkout-wrapper');
  $wrapperCheck.height($heightCheckOut)
  // console.log($heightCheckOut)
  }, 300);

  
});