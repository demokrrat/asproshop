/* eslint-disable no-template-curly-in-string */
/* eslint-disable func-names */
import UIkit from 'uikit';

UIkit.switcher($('.js-tabs-block .default-tabs__nav'), {
  connect: '.js-tabs-block .default-tabs__body',
  toggle: '.js-tabs-block .default-tabs__nav > div',
  animation: 'uk-animation-fade'
});