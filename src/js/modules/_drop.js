/* eslint-disable func-names */
import UIkit from 'uikit';

const dropInNav = $('.js-dropdown-main').each(function (index, element) {
  // element == this
  UIkit.dropdown($(this), {
    pos: 'bottom-center',
    offset: '3'
  });
});

export default dropInNav;