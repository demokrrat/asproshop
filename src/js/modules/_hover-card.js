/* eslint-disable func-names */
$(document).ready(function () {

$( ".card-item" ).hover(
  function() {
    $( this ).closest('.card-slider').addClass('is-hover')
  }, function() {
    $( this ).closest('.card-slider').removeClass('is-hover')
  }
);

$(document).on("click",".mobile-nav__catalog-switcher",function() {
  $(this).next().slideToggle(350)
  $(this).toggleClass('is-active')
  return false
});
  
});