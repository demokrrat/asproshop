/* eslint-disable no-console */
/* eslint-disable func-names */
import Inputmask from "inputmask";

Inputmask({
  "mask": "+38(999) 999-99-99", 
  "clearIncomplete": true,
  "oncomplete": function () {
    if ( $('form.modal-default__footer-call__form').length > 0 ) {
      $('form.modal-default__footer-call__form').addClass('is-complete')
    }
    if ( $('.js-product-phone-send').length > 0 ) {
      $('.js-product-phone-send').addClass('is-complete')
    }
  },
  'autoUnmask': true
}).mask("[type='tel']");



// eslint-disable-next-line no-undef
Inputmask({"mask": "99.99.9999"}).mask(".mask-date");


$(document).on('click','form.modal-default__footer-call__form button',function(){
  // code
  
  const $form = $(this).closest('form');
  const $error = $form.find('.default-form__status');
  
  if ( $('form.modal-default__footer-call__form').hasClass('is-complete') ) {
    $form.removeClass('is-error');
    $error.hide();
  } else {
    $form.addClass('is-error');
    $error.show();
  }
  
  
}); 




