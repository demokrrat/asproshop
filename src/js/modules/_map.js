/* eslint-disable no-loop-func */
/* eslint-disable no-new */
/* eslint-disable no-redeclare */
/* eslint-disable no-use-before-define */
/* eslint-disable no-shadow */
/* eslint-disable vars-on-top */
/* eslint-disable func-names */
/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
/* eslint-disable dollar-sign/dollar-sign */
/* eslint-disable no-var */
let beaches
if ( $('.js-map-set-position').data('places-all') ) {
    beaches = $('.js-map-set-position').data('places-all');
}


  
  
function setMarkers(map) {
    // Adds markers to the map.
    // Marker sizes are expressed as a Size of X,Y where the origin of the image
    // (0,0) is located in the top left of the image.
    // Origins, anchor positions and coordinates of the marker increase in the X
    // direction to the right and in the Y direction down.
    const image = {
      url:
        "/local/templates/s1/img/mapPoint.svg",
      // This marker is 20 pixels wide by 32 pixels high.
      size: new google.maps.Size(47, 63),
      // The origin for this image is (0, 0).
      origin: new google.maps.Point(0, 0),
      // The anchor for this image is the base of the flagpole at (0, 32).
      anchor: new google.maps.Point(84 / 2, 80),
    };
    // Shapes define the clickable region of the icon. The type defines an HTML
    // <area> element 'poly' which traces out a polygon as a series of X,Y points.
    // The final coordinate closes the poly by connecting to the first coordinate.
    const shape = {
      coords: [1, 1, 1, 20, 18, 20, 18, 1],
      type: "poly",
    };

    var bounds = new google.maps.LatLngBounds();
  
    for (let i = 0; i < beaches.length; i++) {
      const beach = beaches[i];
      const marker = new google.maps.Marker({
        position: { lat: beach[1], lng: beach[2] },
        map,
        icon: image,
        shape,
        title: beach[0],
        zIndex: beach[3],
      });

      var position = new google.maps.LatLng(beaches[i][1], beaches[i][2]);


        bounds.extend(position)

      
    }

    map.fitBounds(bounds);
  }

  
// eslint-disable-next-line no-unused-vars
function initMap() {
    // eslint-disable-next-line dollar-sign/dollar-sign
    const mapEl = $("#map");
    const lat = mapEl.data('lat');
    const lng = mapEl.data('lng');
    const title = mapEl.data('title');
    let center;
    var latlng = new google.maps.LatLng(lat, lng);
    var settings = {
        scrollwheel: false,
        zoom: 12,
        mapTypeControl: true,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
        },
        navigationControl: true,
        navigationControlOptions: {
            style: google.maps.NavigationControlStyle.SMALL
        },
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        styles: [
            {
                "stylers": [
                    {
                        "hue": "#2c3e50"
                    },
                    {
                        "saturation": 250
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#e9e9e9"
                    },
                    {
                        "lightness": 17
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#f5f5f5"
                    },
                    {
                        "lightness": 20
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#ffffff"
                    },
                    {
                        "lightness": 17
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#ffffff"
                    },
                    {
                        "lightness": 29
                    },
                    {
                        "weight": 0.2
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#ffffff"
                    },
                    {
                        "lightness": 18
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#ffffff"
                    },
                    {
                        "lightness": 16
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#f5f5f5"
                    },
                    {
                        "lightness": 21
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#dedede"
                    },
                    {
                        "lightness": 21
                    }
                ]
            },
            {
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "color": "#ffffff"
                    },
                    {
                        "lightness": 16
                    }
                ]
            },
            {
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "saturation": 36
                    },
                    {
                        "color": "#333333"
                    },
                    {
                        "lightness": 40
                    }
                ]
            },
            {
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#f2f2f2"
                    },
                    {
                        "lightness": 19
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#fefefe"
                    },
                    {
                        "lightness": 20
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#fefefe"
                    },
                    {
                        "lightness": 17
                    },
                    {
                        "weight": 1.2
                    }
                ]
            }
        ]
    };
    
    var map = new google.maps.Map(document.getElementById("map"),
        settings);
        setMarkers(map);

        $(document).on("click",".js-map-set-position > a",function(e) {
            e.preventDefault();
            const newLat = $(this).data('current-place-point')[0];
            const newLng = $(this).data('current-place-point')[1];
            $('.js-map-set-position > a').removeClass('active');
            $(this).addClass('active')

            // console.log(`${newLat  } - ${  newLng}`)

            map.setZoom(14);
            map.setCenter({lat: newLat, lng: newLng});
        });
            
        
    // var companyLogo = new google.maps.MarkerImage('/img/mapPoint.svg',
    //     new google.maps.Size(47, 63),
    //     new google.maps.Point(0, 0),
    //     new google.maps.Point(84 / 2, 80)
    // );

    // var companyPos = new google.maps.LatLng(lat, lng);
    // var companyMarker = new google.maps.Marker({
    //     position: companyPos,
    //     map,
    //     icon: companyLogo,
    //     title,
    // });

    // google.maps.event.addListener(map, 'idle', function () {
    //     center = map.getCenter();
    // });
    // google.maps.event.addDomListener(window, 'resize', function () {
    //     google.maps.event.trigger(map, 'resize');
    //     map.setCenter(center);
    // });


 


};

$(document).ready(function () {
    window.initMap = initMap;

    if ($("#map").length) {
        const key = $("#map").data('key');
        $("#map").after(`<script src="https://maps.googleapis.com/maps/api/js?key=${key}&sensor=false&language=en&callback=initMap" async defer></script>`);
    };
});