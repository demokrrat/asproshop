/* eslint-disable func-names */
import '../lib/jquery.nicescroll'

$( document ).ready(function() {
    $(".service-map .address-list").niceScroll({
		cursorcolor:"#f8f8f9",
		cursorwidth:"5px",
		background:"#E0E0E0",
		cursorborder:"0px solid #f36d2d",
		cursorborderradius:0,
		cursorfixedheight: 40,
		scrollspeed: 50
	});

});