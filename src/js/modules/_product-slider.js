/* eslint-disable prefer-const */
/* eslint-disable func-names */
/* eslint-disable no-unused-vars */
import {Swiper, Navigation, Scrollbar} from 'swiper';

Swiper.use([Navigation, Scrollbar]);

  // eslint-disable-next-line func-names
$(document).ready(function () {


  if (  $('.card-slider__wrapper').length) {

  
  
  // eslint-disable-next-line no-unused-vars
  $.each( $('.card-slider__wrapper'), function (indexInArray, valueOfElement) { 
  
    
    const $next = $(this).closest('.card-slider__wrapper').find('.js-card-slider__nav-next');
    const $prev = $(this).closest('.card-slider__wrapper').find('.js-card-slider__nav-prev');
    const $scrollbar = $(this).closest('.card-slider__wrapper').find('.swiper-scrollbar');
    const $slider = $(this).find('.js-product-slider');

    // console.log($scrollbar[0])
  
    const slider = new Swiper($slider[0], {
      loop: true,
      slidesPerView: 4,
      watchSlidesProgress: true,
      watchSlidesVisibility: true,
      scrollbar: {
        el: $scrollbar[0],
        hide: false,
        draggable: true
      },
      // Navigation arrows
      navigation: {
        nextEl: $next[0],
        prevEl: $prev[0],
      },
      breakpoints: {
        // when window width is >= 320px
        320: {
          
          slidesPerView: 'auto',
          freeMode: true
        },
        // when window width is >= 480px
        480: {
        },
        // when window width is >= 640px
        767: {
         
          slidesPerView: 3,
          freeMode: false
        },
        1024: {
         
          slidesPerView: 4,
        }
      }
      
    });

    

  });

}
  
});

