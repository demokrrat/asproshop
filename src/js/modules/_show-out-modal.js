import UIkit from 'uikit';

const showPopupOut = (elem) => {
  UIkit.modal(elem).show();
}
window.showPopupOut = showPopupOut;


const hidePopupOut = (elem) => {
  UIkit.modal(elem).hide();
}
window.hidePopupOut = hidePopupOut;


