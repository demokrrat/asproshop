/* eslint-disable no-unused-vars */
/* eslint-disable func-names */

import 'slick-carousel';

$('slider').each(function (index, element) {
  // element == this

  $(this).slick()
  
});





$( document ).ready(function() {
  $('.js-slid-big').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
   // arrows: false,
    fade: false,
    autoplay: false,
    adaptiveHeight: false,
    infinite: false,
    nextArrow: $('.slider-nav-wrap .next'),
    prevArrow: $('.slider-nav-wrap .prev'),
    asNavFor: '.js-slick-nav',
    responsive: [			
      {
          breakpoint: 769,
          settings: {
          infinite: true,
          adaptiveHeight: true
          }
      }			
    ]
  });
  $('.js-slick-nav').slick({
    slidesToShow: 6,
    slidesToScroll: 6,
    asNavFor: '.js-slid-big',
    arrows: false,
    focusOnSelect: true,
    fade: false,
    infinite: false, 
    vertical: true,
    draggable: true,
    adaptiveHeight: false,
    verticalSwiping: true,
    swipe: false,	  
    swipeToSlide: false,
      responsive: [
      {
        breakpoint: 1441,
        settings: {
          slidesToShow: 4, 
          slidesToScroll: 4,
          adaptiveHeight: true
        }
        },
      {
            breakpoint: 769,
            settings: {
          vertical: false,
          infinite: true,
          swipe: true,	  
          // swipeToSlide: true,
          
              }
      }			
    ]
  }); 

  $('.js-regist-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
   // arrows: false,
    fade: false,
    autoplay: false,
    adaptiveHeight: false,
    infinite: false,
    dots: true,
    arrows: false
  });

  /* */
  $('.why-we .why-list').slick({
   
    
      responsive: [
        {
          breakpoint: 3500,
          settings: "unslick"
        }	,
      {
        breakpoint: 970,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          arrows: false,
          dots: true,
          focusOnSelect: true,
          infinite: true, 
          swipe: true,	  
          swipeToSlide: true,
        }
        },
        {
          breakpoint: 580,
          settings: {
            slidesToShow: 1,
            arrows: false,
            dots: true,
          }
        }			
    ]
  }); 
  /* */


  $( ".share-wrap .other-view" ).click(function() {
    $(this).next('.share-link').toggleClass('open');
  });

  $( ".view-all" ).click(function(e) {
    e.preventDefault();
     $(this).siblings('.more-five').toggleClass('open');  
     $(this).toggleClass('active');  
  });


  $('.product-size-list').each(function(){ 	
    const sizeLenght = $(this).children('.product-size-item').length;
    // eslint-disable-next-line no-console
    console.log(sizeLenght);
    if (sizeLenght > 4){
      $(this).addClass('more-five');
    }
  });


  $('.play-btn').on('click', function(e) {
    e.preventDefault();
    const parentVideo = $(this).parents('.video-wrapper');
    parentVideo.find(".videoIframe")[0].src += "?autoplay=1";
    parentVideo.find('.videoIframe').show();
    parentVideo.find('.video-cover').hide();
    parentVideo.find('.play-btn').hide();
  })
  
  $('.top-product-detail .l-section .play-video').on('click', function(e) {
    e.preventDefault();
    $('.tabs-wrap .tabs-nav__item ').removeClass('uk-active');
    $('.tabs-wrap .tabs-content__item').removeClass('uk-active');
    $('.tabs-wrap .tabs-nav__item-video').addClass('uk-active');
    $('.tabs-content .tabs-content__item-video').addClass('uk-active');
    $('html, body').animate({scrollTop: $('.tabs-content__item-video').offset().top - 70}, 1500);

  });


  $('.js-top-review').on('click', function(e) {
    e.preventDefault();
    $('.tabs-wrap .tabs-nav__item ').removeClass('uk-active');
    $('.tabs-wrap .tabs-content__item').removeClass('uk-active');
    $('.tabs-wrap .tabs-nav__item-review').addClass('uk-active');
    $('.tabs-content .tabs-content__item-review').addClass('uk-active');
    $('html, body').animate({scrollTop: $('.tabs-content__item-review').offset().top - 70}, 1500);

  });

  $('.nav-review .number-responses').on('click', function(e) {
    $(this).next('.responses-wrap').slideToggle();
  });

  $('.nav-review .nav-reply').on('click', function(e) {
    e.preventDefault();
    $(this).siblings('.reply-to-review').slideToggle();
  })

  $('.form-wrap-bg .add-comment').on('click', function(e) {
    e.preventDefault();
    $(this).next('.hide-comment').slideToggle();
  })

  $('.row-history .col-title.arrow').on('click', function(e) {
    const parentsBlock = $(this).parents('.row-history');
    parentsBlock.find('.hide-block').slideToggle();
    parentsBlock.toggleClass('open');
  });

  $('.about-top .btn__red').click(function () {
    $('body,html').animate({
        scrollTop: $('.about-page').offset().top
    }, 1500);
    return false;})


  function checkWidth() {
    const windowWidth = $('body').innerWidth(); 
    
	  if(windowWidth < 767){
      $('.product-about__item .title').on('click', function(e) {
        e.preventDefault();
        $(this).next('ul').toggleClass('open');
        $(this).parents('.product-about__item').toggleClass('open');
      })
	  }
	}
  
	checkWidth(); // проверит при загрузке страницы
  
	$(window).resize(function(){
	  checkWidth(); // проверит при изменении размера окна клиента
	});
 


});
