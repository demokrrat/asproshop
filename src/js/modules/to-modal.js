/* eslint-disable eqeqeq */
/* eslint-disable func-names */
/* eslint-disable prefer-const */
import UIkit from 'uikit';

const createModal = (content, id) => {
  let $wrapper = '';
  $wrapper = `<div id="${  id  }" class="hide-desktop-modal uk-flex-top" uk-modal>\n` +
    `                <div class="modal-default uk-modal-dialog uk-modal-body uk-margin-auto-vertical modal-default__no-gap">\n` +
    `                  <button class="uk-modal-close-default" type="button" uk-close></button>\n` +
    `                  <div class="modal-default__inner">\n` +
      `                    ${ content }\n` +
    `                  </div>\n` +
    `                </div>\n` + 
    `              </div>`;
  let $elem = '';

  if ( !$('.mobile-modals').length ) {
    $elem = document.createElement("div");
    $($elem).addClass('mobile-modals');
    $($elem).appendTo('body');

    console.log(1)
  }

  console.log(0)
  let $item = document.createElement("div");
  
  $($item).addClass(`mobile-modals__item ${  id  } `);
  $($item).appendTo('.mobile-modals');

  $($item).html($wrapper);
  $('.hide-desktop-modal').find('.nice-select').addClass('nice-select__inline')

  $(document).on("click",`#${ id  }`,function(e) {
    console.log($(e.target))
    if ( $(e.target).hasClass('option') ) {
      let $itemSelect = $(e.target).index();
      let $itemSelectText = $(e.target).text();
      $(`[data-list-name=${  id  }]`).find(`option:eq(${ $itemSelect })`).attr('selected', 'selected');
      $(`[data-list-name=${  id  }]`).trigger('change');
      $(`[data-list-name=${  id  }]`).next('.nice-select').find('.current').text($itemSelectText)
    
      console.log(11111)
    }
    UIkit.modal(`#${  id  }`).hide();
  });

  
  // console.log(id)

  // UIkit.modal(`#${  id  }`).show();





}

export default createModal;